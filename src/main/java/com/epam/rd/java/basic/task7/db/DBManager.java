package com.epam.rd.java.basic.task7.db;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
    }

    // //////////////////////////////////////////////////////////
    // SQL queries
    // //////////////////////////////////////////////////////////

    private static final String SQL_FIND_USER = "SELECT * FROM users WHERE login=?";
    private static final String SQL_FIND_ALL_USERS = "SELECT * FROM users ORDER BY id";
    private static final String SQL_INSERT_USER = "INSERT INTO users (id, login) VALUES (DEFAULT, ?)";
    private static final String SQL_DELETE_USER = "DELETE FROM users WHERE login = ?";

    private static final String SQL_FIND_TEAM = "SELECT * FROM teams WHERE name=?";
    private static final String SQL_FIND_ALL_TEAMS = "SELECT * FROM teams ORDER BY id";
    private static final String SQL_INSERT_TEAM = "INSERT INTO teams (id, name) VALUES (DEFAULT, ?)";
    private static final String SQL_DELETE_TEAM = "DELETE FROM teams WHERE name = ?";
    private static final String SQL_UPDATE_TEAM = "UPDATE teams SET name = ? WHERE id = ?";

    private static final String SQL_SET_TEAM_FOR_USER = "INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)";
    private static final String SQL_FIND_USER_TEAMS = "SELECT * FROM teams t, users_teams ut WHERE ut.user_id = ? AND t.id = ut.team_id";


    /**
     * Returns a DB connection from the Pool Connections. Before using this
     * method you must configure the Date Source and the Connections Pool in
     * your WEB_APP_ROOT/META-INF/context.xml file.
     *
     * @return DB connection.
     */
    private static Connection getConnection() throws DBException {

        FileReader fileReader = null;
        try {
            fileReader = new FileReader("app.properties");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Properties prop = new Properties();
        try {
            prop.load(fileReader);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String FULL_URL = prop.getProperty("connection.url");

        try {
            //Connection con =  DriverManager.getConnection(URL, USER, PASS);
            return DriverManager.getConnection(FULL_URL);
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e.getCause());
        }
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();

        try (Connection con = getConnection(); Statement stmt = con.createStatement(); ResultSet rs = stmt.executeQuery(SQL_FIND_ALL_USERS)) {

            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("id"));
                user.setLogin(rs.getString("login"));
                users.add(user);
            }
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e.getCause());
        }


        return users;
    }

    public boolean insertUser(User user) throws DBException {
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = DBManager.getConnection();
            stmt = con.prepareStatement(SQL_INSERT_USER, Statement.RETURN_GENERATED_KEYS);

            stmt.setString(1, user.getLogin());
            int count = stmt.executeUpdate();
            if (count > 0) {
                try (ResultSet rs = stmt.getGeneratedKeys()){
                    if (rs.next()) {
                        user.setId(rs.getInt(1));

                    }
                }
            }

        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e.getCause());
        } finally {
            close(con);
            close(stmt);
        }

        return true;
    }

    private void close(AutoCloseable stmt) {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public boolean deleteUsers(User... users) throws DBException {
        Connection con = null;
        PreparedStatement stmt = null;

        try {
            con = DBManager.getConnection();
            stmt = con.prepareStatement(SQL_DELETE_USER);

            for (User user : users) {
                stmt.setString(1, user.getLogin());
                stmt.executeUpdate();
            }


        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e.getCause());
        } finally {
            close(con);
            close(stmt);
        }

        return true;
    }

    public User getUser(String login) throws DBException {
        User user = new User();

        try (Connection con = getConnection(); PreparedStatement stmt = con.prepareStatement(SQL_FIND_USER)) {

            stmt.setString(1, login);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                user.setId(rs.getInt("id"));
                user.setLogin(rs.getString("login"));
            }
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e.getCause());
        }

        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = new Team();

        try (Connection con = getConnection(); PreparedStatement stmt = con.prepareStatement(SQL_FIND_TEAM)) {

            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                team.setId(rs.getInt("id"));
                team.setName(rs.getString("name"));
            }
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e.getCause());
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();

        try (Connection con = getConnection(); Statement stmt = con.createStatement(); ResultSet rs = stmt.executeQuery(SQL_FIND_ALL_TEAMS)) {

            while (rs.next()) {
                Team team = new Team();
                team.setId(rs.getInt("id"));
                team.setName(rs.getString("name"));
                teams.add(team);
            }
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e.getCause());
        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = DBManager.getConnection();
            stmt = con.prepareStatement(SQL_INSERT_TEAM, Statement.RETURN_GENERATED_KEYS);

            stmt.setString(1, team.getName());
            int count = stmt.executeUpdate();
            if (count > 0) {
                try (ResultSet rs = stmt.getGeneratedKeys()){
                    if (rs.next()) {
                        team.setId(rs.getInt(1));

                    }
                }
            }

        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e.getCause());
        } finally {
            close(con);
            close(stmt);
        }

        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = DBManager.getConnection();
            con.setAutoCommit(false);
            stmt = con.prepareStatement(SQL_SET_TEAM_FOR_USER);

            for (Team team : teams) {
                stmt.setInt(1, user.getId());
                stmt.setInt(2, team.getId());
                stmt.executeUpdate();
            }
            con.commit();
        } catch (SQLException e) {
            rollback(con);
            throw new DBException(e.getMessage(), e.getCause());
        } finally {
            close(con);
            close(stmt);
        }

        return true;
    }

    private void rollback(Connection con) throws DBException {
        try {
            con.rollback();
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex.getCause());
        }
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teams = new ArrayList<>();
        PreparedStatement stmt = null;

        try (Connection con = getConnection()) {

            stmt = con.prepareStatement(SQL_FIND_USER_TEAMS);
            stmt.setInt(1, user.getId());
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                teams.add(getTeam(rs.getString("name")));

            }

        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e.getCause());
        } finally {
            close(stmt);
        }
        return teams;
    }

    public boolean deleteTeam(Team team) throws DBException {

        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = DBManager.getConnection();
            stmt = con.prepareStatement(SQL_DELETE_TEAM);
            stmt.setString(1, team.getName());
            stmt.executeUpdate();

        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e.getCause());
        } finally {
            close(con);
            close(stmt);
        }

        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = DBManager.getConnection();
            stmt = con.prepareStatement(SQL_UPDATE_TEAM);
            stmt.setString(1, team.getName());
            stmt.setInt(2, team.getId());
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e.getCause());
        } finally {
            close(stmt);
            close(con);
        }
        return true;
    }
}
